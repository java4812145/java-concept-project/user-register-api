import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.roberto.qa.Utils;

import static org.roberto.qa.Utils.retornaMensagem;

public class HelloWorldTest {
    @Tag("funcional")
    @Test
    public void exibirMensagemHelloWorld() {
        System.out.println("Executou funcional...");
    }

    @Tag("regressao")
    @Test
    public void exibirMensagemregressao() {
        System.out.println("Executou regressao...");
    }

    @Tag("exploratorio")
    @Test
    public void exibirMensagemExploratorio() {

        System.out.println("Executou exploratorio...");
    }
}